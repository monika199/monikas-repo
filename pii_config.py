# Define all configuration required for Scrubbing the data.
# pii_config is a dictionary containing all the field name path and its scrubbed type.
# Field name path must be absolute path for the value and it must start with the table name.
# Import required libraries
from enum import Enum
import csv
import uuid
import random
import os
import time

# Enum for Scrub type
class ScrubType(Enum):
    NAME = "name"
    LAST_NAME = "last_name"
    PHONE = "phone"
    EXP_YEAR = "exp_year"
    LAST_FOUR = "last_four"
    EMAIL = "email"
    EXP_MONTH = "exp_month"
    DOB = "dob"
    NOTES = "notes"


PII_CONFIG = {
    # Table policies.policy_info
    "policies.charges.sum": ScrubType.NAME.value,
    "policies.first_name": ScrubType.NAME.value,
    "policies.last_name": ScrubType.LAST_NAME.value,
    "policies.middle_name": ScrubType.NAME.value,
    "policies.date_of_birth": ScrubType.DOB.value,
    "policies.email": ScrubType.EMAIL.value,
    "policies.phone_number": ScrubType.PHONE.value,
    "policies.policy_info.personal_information.first_name": ScrubType.NAME.value,
    "policies.policy_info.personal_information.last_name": ScrubType.LAST_NAME.value,
    "policies.policy_info.delta.property_data.mortgages.details.0.email.after": ScrubType.EMAIL.value,
    "policies.policy_info.delta.base_update.property_data.mortgages.details.0.phone_number": ScrubType.PHONE.value,
    "policies.policy_info.delta.base_update.property_data.mortgages.details." \
    "0.phone_number.after": ScrubType.PHONE.value,
    "policies.policy_info.delta.property_data.mortgages.details.0.phone_number": ScrubType.PHONE.value,
    "policies.policy_info.delta.property_data.mortgages.details.0.phone_number.after": ScrubType.PHONE.value,
    "policies.policy_info.delta.property_data.mortgages.details.0.phone_number.before": ScrubType.PHONE.value,
    "policies.policy_info.reports.inspection.property_information.insured_cell_phone": ScrubType.PHONE.value,
    "policies.policy_info.reports.inspection.property_information.insured_home_phone": ScrubType.PHONE.value,
    "policies.policy_info.reports.inspection.property_information.insured_office_phone": ScrubType.PHONE.value,
    "policies.policy_info.reports.inspection.property_information.insured_secondary_phone": ScrubType.PHONE.value,
    "policies.policy_info.reports.inspection.property_information.insured_email": ScrubType.EMAIL.value,
    "policies.data.reports.inspection.property_information.insured_last_name": ScrubType.NAME.value,
    "policies.data.reports.inspection.property_information.insured_first_name": ScrubType.NAME.value,
    "policies.policy_info.checkout_data.cc_info.name": ScrubType.NAME.value,
    "policies.policy_info.personal_information.middle_name": ScrubType.NAME.value,
    "policies.policy_info.personal_information.second_insured.last_name": ScrubType.NAME.value,
    "policies.policy_info.personal_information.second_insured.first_name": ScrubType.NAME.value,
    "producers.data.name": ScrubType.NAME.value,
    "producers.data.producer_alternative_email": ScrubType.EMAIL.value,
    "producers.data.phone": ScrubType.PHONE.value,
    "producers.email": ScrubType.EMAIL.value,
    "organizations.data.bank.name": ScrubType.NAME.value,
    "organizations.data.contact.name": ScrubType.NAME.value,
    "organizations.data.contact.email": ScrubType.EMAIL.value,
    "organizations.data.contact.phone": ScrubType.PHONE.value,
    "organizations.data.relationship_manager.partner_name": ScrubType.NAME.value,
    "organizations.data.relationship_manager.partner_email": ScrubType.EMAIL.value,
    "organizations.data.relationship_manager.partner_phone": ScrubType.PHONE.value,
    "organizations.name": ScrubType.NAME.value,
    "owners.email": ScrubType.EMAIL.value,
    "owners.first_name": ScrubType.NAME.value,
    "owners.last_name": ScrubType.NAME.value,
    "owners.middle_name": ScrubType.NAME.value,
    "leads.email": ScrubType.EMAIL.value,
    "leads.first_name": ScrubType.NAME.value,
    "leads.middle_name": ScrubType.NAME.value,
    "leads.last_name": ScrubType.NAME.value
}


def get_name_key():
    name_keys = list()
    for k,v in PII_CONFIG.items():
        if v == "name":
            name_keys.append(k)
    return name_keys

def create_csv_row(csv_row):
        return csv_row+[my_random_string() for _ in range(0, 13)]

def my_random_string():
    random = str(uuid.uuid4())
    random = random.upper()
    random = random.replace("-","")
    al_num = random[0:6]
    if type(al_num) != int:
        return al_num
    else:
        my_random_string()

def create_csv():
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(BASE_DIR, 'files', f'output{int(time.time())}.csv'), 'a+') as f:
        outputWriter = csv.writer(f)
        name_keys = get_name_key()
        for one_name in range(len(name_keys)):
            csv_row = ["S#"]
            csv_row.append(name_keys[one_name])
            outputWriter.writerow(create_csv_row(csv_row))

if __name__=="__main__":
    create_csv()
